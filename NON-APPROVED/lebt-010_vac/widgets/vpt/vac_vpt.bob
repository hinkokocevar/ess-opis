<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>VPT</name>
  <macros>
    <vacPREFIX>$(SEC)-$(SUBSEC):$(DIS)-$(DEV)-$(IDX)</vacPREFIX>
    <vacROOT>$(ROOT=/ess-opis/NON-APPROVED)</vacROOT>
  </macros>
  <width>133</width>
  <height>70</height>
  <scripts>
    <script file="../common/isvalid.py" check_connections="false">
      <pv_name>$(vacPREFIX):ValidR</pv_name>
    </script>
    <script file="error.js">
      <pv_name>$(vacPREFIX):ErrorR</pv_name>
      <pv_name>$(vacPREFIX):ErrorCodeR</pv_name>
      <pv_name trigger="false">loc://$(vacPREFIX):error("")</pv_name>
    </script>
    <script file="warning.js">
      <pv_name>$(vacPREFIX):WarningR</pv_name>
      <pv_name>$(vacPREFIX):WarningCodeR</pv_name>
      <pv_name trigger="false">loc://$(vacPREFIX):warning("")</pv_name>
    </script>
  </scripts>
  <widget type="polyline" version="2.0.0">
    <name>PIPE_LEFT</name>
    <width>46</width>
    <height>50</height>
    <visible>$(PIPE_LEFT=false)</visible>
    <points>
      <point x="0.0" y="25.0">
      </point>
      <point x="46.0" y="25.0">
      </point>
    </points>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>PIPE_RIGHT</name>
    <x>96</x>
    <width>37</width>
    <height>50</height>
    <visible>$(PIPE_RIGHT=false)</visible>
    <points>
      <point x="0.0" y="25.0">
      </point>
      <point x="37.0" y="25.0">
      </point>
    </points>
  </widget>
  <widget type="polyline" version="2.0.0">
    <name>PIPE_BELOW</name>
    <x>46</x>
    <y>50</y>
    <width>50</width>
    <visible>$(PIPE_BELOW=false)</visible>
    <points>
      <point x="25.0" y="0.0">
      </point>
      <point x="25.0" y="20.0">
      </point>
    </points>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>Symbol</name>
    <pv_name>loc://$(vacPREFIX):UI:StatusR&lt;VEnum&gt;(0, "Invalid", "Accelerating", "At Nominal Speed", "Stopped", "Error")</pv_name>
    <symbols>
      <symbol>$(vacROOT)/COMMON/DEVICES/vacuum/vpt/vpt-invalid.png</symbol>
      <symbol>$(vacROOT)/COMMON/DEVICES/vacuum/vpt/vpt-acceleration.png</symbol>
      <symbol>$(vacROOT)/COMMON/DEVICES/vacuum/vpt/vpt-nominal-speed.png</symbol>
      <symbol>$(vacROOT)/COMMON/DEVICES/vacuum/vpt/vpt-off.png</symbol>
      <symbol>$(vacROOT)/COMMON/DEVICES/vacuum/vpt/vpt-error.png</symbol>
    </symbols>
    <x>46</x>
    <width>50</width>
    <height>50</height>
    <background_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </background_color>
    <rules>
      <rule name="Speed" prop_id="tooltip" out_exp="true">
        <exp bool_exp="True">
          <expression>pvStr0</expression>
        </exp>
        <pv_name>$(CONTROLLER):SpdR</pv_name>
      </rule>
    </rules>
    <scripts>
      <script file="symbol_color_animation.js" check_connections="false">
        <pv_name trigger="false">loc://$(vacPREFIX):UI:StatusR</pv_name>
        <pv_name>$(vacPREFIX):AcceleratingR</pv_name>
        <pv_name>$(vacPREFIX):AtNominalSpdR</pv_name>
        <pv_name>$(vacPREFIX):StoppedR</pv_name>
        <pv_name>$(vacPREFIX):ErrorR</pv_name>
      </script>
    </scripts>
    <tooltip>N/A</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Device</name>
    <text>$(DEV)</text>
    <width>40</width>
    <font>
      <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
      </font>
    </font>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Index</name>
    <text>$(IDX)</text>
    <y>30</y>
    <width>40</width>
    <font>
      <font name="TINY-MONO-PLAIN" family="Source Code Pro" style="REGULAR" size="12.0">
      </font>
    </font>
    <horizontal_alignment>2</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Open Faceplate</name>
    <actions>
      <action type="open_display">
        <file>../../faceplates/vpt/vac-vpt-popup.bob</file>
        <target>standalone</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <width>46</width>
    <height>50</height>
    <transparent>true</transparent>
    <tooltip>Open faceplate</tooltip>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Interlock Group</name>
    <x>46</x>
    <y>52</y>
    <width>16</width>
    <height>16</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="symbol" version="2.0.0">
      <name>Interlock_badge</name>
      <pv_name>loc://$(vacPREFIX):UI:ITLckR&lt;VEnum&gt;(0, "Invalid", "Healthy", "Tripped", "Overriden", "Disabled")</pv_name>
      <symbols>
        <symbol>../symbols/interlock/interlock-invalid.png</symbol>
        <symbol>../symbols/interlock/interlock-healthy.png</symbol>
        <symbol>../symbols/interlock/interlock-tripped.png</symbol>
        <symbol>../symbols/interlock/interlock-overridden.png</symbol>
        <symbol>../symbols/interlock/interlock-disabled.png</symbol>
      </symbols>
      <width>16</width>
      <height>16</height>
      <scripts>
        <script file="../common/interlock_color_animation.js" check_connections="false">
          <pv_name trigger="false">$(pv_name)</pv_name>
          <pv_name>$(vacPREFIX):ITLck:HltyR</pv_name>
          <pv_name>$(vacPREFIX):ITLck:TrpR</pv_name>
          <pv_name>$(vacPREFIX):ITLck:OvRidnR</pv_name>
          <pv_name>$(vacPREFIX):ITLck:DisR</pv_name>
        </script>
      </scripts>
      <tooltip>$(pv_value)</tooltip>
      <border_alarm_sensitive>false</border_alarm_sensitive>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Interlock</name>
      <actions>
        <action type="open_display">
          <file>../../faceplates/common/vac_vpp-vpt-interlocks.bob</file>
          <target>standalone</target>
        </action>
      </actions>
      <text></text>
      <width>16</width>
      <height>16</height>
      <transparent>true</transparent>
      <rules>
        <rule name="Tooltip" prop_id="tooltip" out_exp="true">
          <exp bool_exp="true">
            <expression>pvStr0</expression>
          </exp>
          <pv_name>loc://$(vacPREFIX):UI:ITLckR</pv_name>
        </rule>
      </rules>
      <tooltip>No Interlock</tooltip>
    </widget>
  </widget>
  <widget type="picture" version="2.0.0">
    <name>Badge Locked</name>
    <file>../symbols/badges/locked.png</file>
    <x>99</x>
    <width>16</width>
    <height>16</height>
    <visible>false</visible>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="true">
        <exp bool_exp="true">
          <expression>pvInt0</expression>
        </exp>
        <pv_name>$(vacPREFIX):AutoStartDisStR</pv_name>
      </rule>
    </rules>
    <tooltip>Automatic Start Disabled</tooltip>
  </widget>
  <widget type="picture" version="2.0.0">
    <name>Badge Local</name>
    <file>../symbols/badges/local.png</file>
    <x>116</x>
    <width>16</width>
    <height>16</height>
    <visible>false</visible>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="true">
        <exp bool_exp="true">
          <expression>pvInt0</expression>
        </exp>
        <pv_name>$(vacPREFIX):LocalControlR</pv_name>
      </rule>
    </rules>
    <tooltip>Local Control Enabled</tooltip>
  </widget>
  <widget type="picture" version="2.0.0">
    <name>Badge Started</name>
    <file>../symbols/badges/started.png</file>
    <x>99</x>
    <y>34</y>
    <width>16</width>
    <height>16</height>
    <visible>false</visible>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="true">
        <exp bool_exp="true">
          <expression>pvInt0</expression>
        </exp>
        <pv_name>$(vacPREFIX):StartDQ-RB</pv_name>
      </rule>
    </rules>
    <tooltip>Started</tooltip>
  </widget>
  <widget type="picture" version="2.0.0">
    <name>Badge Manual</name>
    <file>../symbols/badges/manual.png</file>
    <x>116</x>
    <y>34</y>
    <width>16</width>
    <height>16</height>
    <visible>false</visible>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="true">
        <exp bool_exp="true">
          <expression>pvInt0</expression>
        </exp>
        <pv_name>$(vacPREFIX):ManualModeR</pv_name>
      </rule>
    </rules>
    <tooltip>Manual Control Enabled</tooltip>
  </widget>
  <widget type="group" version="2.0.0">
    <name>Warning Group</name>
    <x>99</x>
    <y>52</y>
    <width>16</width>
    <height>16</height>
    <visible>false</visible>
    <style>3</style>
    <transparent>true</transparent>
    <rules>
      <rule name="Visibility" prop_id="visible" out_exp="true">
        <exp bool_exp="true">
          <expression>pvInt0</expression>
        </exp>
        <pv_name>$(vacPREFIX):WarningR</pv_name>
      </rule>
    </rules>
    <tooltip>No Warning Message</tooltip>
    <widget type="picture" version="2.0.0">
      <name>Badge Warning</name>
      <file>../symbols/badges/warning.png</file>
      <width>16</width>
      <height>16</height>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Warning</name>
      <actions>
        <action type="open_display">
          <file>../../faceplates/common/vac_warning_error.bob</file>
          <target>standalone</target>
        </action>
      </actions>
      <text></text>
      <width>16</width>
      <height>16</height>
      <transparent>true</transparent>
      <rules>
        <rule name="Tooltip" prop_id="tooltip" out_exp="true">
          <exp bool_exp="True">
            <expression>pvStr0</expression>
          </exp>
          <pv_name>loc://$(vacPREFIX):warning("")</pv_name>
        </rule>
      </rules>
      <tooltip>No Warning Message</tooltip>
    </widget>
  </widget>
</display>
