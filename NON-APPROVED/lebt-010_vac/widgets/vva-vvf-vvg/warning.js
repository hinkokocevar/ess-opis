PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var warningMsg  = "";
var warningCode = 0;

if (PVUtil.getLong(pvs[0]))
	warningCode = PVUtil.getLong(pvs[1]);

switch (warningCode) {
	case 99:
		warningMsg = "Valve Not Open / Device Not On";
		break;
	case 98:
		warningMsg = "Bypass Interlock Time-Out";
		break;
	case 97:
		warningMsg = "Open Interlock Active (Valve Closed)";
		break;
	case 96:
		warningMsg = "Valve Opening Prevented by Tripped Interlock";
		break;
	case 15:
		warningMsg = "Open Interlock Active (Valve Open)";
		break;
	case 14:
		warningMsg = "Pressure Interlock No. 1 Bypassed / Overridden";
		break;
	case 13:
		warningMsg = "Hardware Interlock No. 1 Bypassed / Overridden";
		break;
	case 12:
		warningMsg = "Pressure Interlock No. 2 Bypassed / Overridden";
		break;
	case 11:
		warningMsg = "Hardware Interlock No. 2 Bypassed / Overridden";
		break;
	case 10:
		warningMsg = "Hardware Interlock No. 3 Bypassed / Overridden";
		break;
	case 9:
		warningMsg = "Software Interlock Bypassed / Overridden";
		break;
	case 8:
		warningMsg = "Open Interlock Active (Valve Closed)";
		break;
	case 7:
		warningMsg = "Valve Opening Prevented by Tripped Interlock";
		break;

	case 0:
		break;
	default:
		warningMsg = "Warning Code: " + PVUtil.getString(pvs[1]);
		break;
}
Logger.info(pvs[1] + " Warning code: " + warningCode + " message: " + warningMsg);

try {
	pvs[2].setValue(warningMsg);
} catch (err) {
	Logger.warning(err);
}
