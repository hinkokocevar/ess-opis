PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var pvOpen      = 0;
var pvClosed    = 0;

var pvSymbol    = pvs[0];

var sum     = 0;
var isValid = 0;
var colorID = 0;

function log_pv(pv) {
	Logger.info(pv + ": " + PVUtil.getString(pv));
}

try {
	pvOpen      = 1 * PVUtil.getInt(pvs[1]);
	pvClosed    = 2 * PVUtil.getInt(pvs[2]);

	sum         = pvOpen | pvClosed;
	isValid     = (sum & (sum - 1)) == 0 ? 1 : 0;

	log_pv(pvs[1]);
	log_pv(pvs[2]);

	if (isValid == 0) {
		Logger.severe(pvSymbol + ": Invalid combination");
	} else if (pvClosed) {
		Logger.info(pvSymbol + ": CLOSED");
		colorID = 3;
	} else if (pvOpen) {
		Logger.info(pvSymbol + ": OPEN");
		colorID = 2;
	} else
		Logger.severe(pvSymbol + ": Unknown combination:" + sum);
} catch (err) {
	Logger.severe("NO CONNECTION: " + err);
}


pvSymbol.write(colorID);
