PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var errorMsg  = "";
var errorCode = 0;

if (PVUtil.getLong(pvs[0]))
	errorCode = PVUtil.getLong(pvs[1]);

switch (errorCode) {
	case 99:
		errorMsg = "Hardware Interlock";
		break;
	case 98:
		errorMsg = "Software Interlock";
		break;
	case 97:
		errorMsg = "Turbo-Pumps Controller Error";
		break;
	case 96:
		errorMsg = "Power Supply Error";
		break;
	case 95:
		errorMsg = "High Vacuum Manifold Vented - Pressure Interlock";
		break;
	case 94:
		errorMsg = "Low Vacuum Manifold Vented - Pressure Interlock";
		break;
	case 93:
		errorMsg = "93";
		break;
	case 92:
		errorMsg = "Primary Pump Error";
		break;
	case 91:
		errorMsg = "Primary Pump / Valve Error";
		break;
	case 90:
		errorMsg = "Turbo-Pumps Not Available";
		break;
	case 89:
		errorMsg = "Max Auto-Restart";
		break;
	case 88:
		errorMsg = "Back-Up Primary Pumping System Error";
		break;
	case 15:
		errorMsg = "Mode Selection Error - Vacuum Sector Vented";
		break;
	case 14:
		errorMsg = "Mode Selection Error - Vacuum Sector Under Vacuum";
		break;
	case 13:
		errorMsg = "Primary Pumping System: Rescue Primary System is Off";
		break;
	case 12:
		errorMsg = "Primary Pumping System: Rescue Primary Not Available";
		break;
	case 11:
		errorMsg = "Primary Pumping System: Primary Pump / Valve Error";
		break;
	case 10:
		errorMsg = "Primary Pumping System: Start Disable";
		break;

	case 0:
		break;
	default:
		errorMsg = "Error Code: " + PVUtil.getString(pvs[1]);
		break;
}
Logger.info(pvs[1] + " Error code: " + errorCode + " message: " + errorMsg);

try {
	pvs[2].setValue(errorMsg);
} catch (err) {
	Logger.warning(err);
}
