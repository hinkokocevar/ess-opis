PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var show_pipe = PVUtil.getString(pvs[0]);
Logger.info("SHOW_PIPE: " + show_pipe);
try {
	ScriptUtil.findWidgetByName(widget, "PIPE_" + show_pipe.toUpperCase()).setPropertyValue("visible", "true");
} catch (err) {
	Logger.severe(err)
}
