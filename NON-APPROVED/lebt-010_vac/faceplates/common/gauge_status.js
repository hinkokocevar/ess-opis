PVUtil = org.csstudio.display.builder.runtime.script.PVUtil;
ScriptUtil = org.csstudio.display.builder.runtime.script.ScriptUtil;
Logger = org.csstudio.display.builder.runtime.script.ScriptUtil.getLogger();

var valid = pvs[0];
var type  = pvs[1];
var chan  = pvs[2];

widget.setPropertyValue("text", PVUtil.getString(type) + " @ " + PVUtil.getString(chan));
widget.setPropertyValue("tooltip", "Gauge " + PVUtil.getString(type) + " @ channel " + PVUtil.getString(chan));
widget.setPropertyValue("transparent", PVUtil.getLong(valid) ? "true" : "false");
