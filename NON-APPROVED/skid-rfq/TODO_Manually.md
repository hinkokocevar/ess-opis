TODO MANUALLY
=============

Things that still need doing manually after generating the OPIs from DXF files.

## Main View
+ Change PDelta (in Vane regulation loop) **Widget Type** from *Text Input* to *Text Update*, **Background Color** to *black (0,0,0)* and **Foreground Color** to *yellow (240,240,0)*
+ Remove artifacts over *PP10*
+ Remove artifacts between *Mixed Tank* and *Body S1-S2 regulation loop*
+ Remove wave artifact in *Mixed Tank*

## Regulations
+ For all 18 PID settings set **Maximum** to *200* and **Minimum** to *0*
+ For 5 setpoints (all but DPhase regulation) **Text Input box** set **Maximum** to *50* (°C) and **Minimum** to *0* (°C)
+ Change PDelta measure (in Vane regulation loop) **Widget Type** from *Text Input* to *Text Update*

## Errors
*NA*
